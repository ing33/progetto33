CREATE TABLE `Client` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  `name` varchar(255) default NULL,
  `surname` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `phone` varchar(100) default NULL,
  `city_birth` varchar(255),
  `date_birth` varchar(255),
  `city_residence` varchar(255),
  `zip` varchar(10) default NULL,
  `street` varchar(255) default NULL,
  `civic_number` mediumint default NULL,
  `CF` varchar(255)
);

INSERT INTO `Client` (`name`,`surname`,`email`,`phone`,`city_birth`,`date_birth`,`city_residence`,`zip`,`street`,`civic_number`,`CF`) VALUES 
("Giacomo","Moody","eu.erat.semper@lacusMaurisnon.net","0812138039","Fresia","1986-06-02","Gontrode","33470","285 Pretium Rd.",42,"PGIEFU00E00M558R"),
("Aristotle","Marsh","at.auctor.ullamcorper@malesuada.org","0810711431","Aurangabad","1996-04-15","Fatehpur","76188","P.O. Box 202, 7022 In Av.",91,"ENMTSE93O20X753O"),
("Forrest","Mccray","nec.luctus@Suspendisse.ca","3926342808","Brighton","1980-05-10","Wandre","99735","934 Mauris, Avenue",70,"FGQUWR59Q56G143E"),
("Deborah","Harvey","egestas@vestibulumMaurismagna.ca","0815905339","Temuka","1997-09-04","Corswarem","18178","859-5972 Fermentum Av.",30,"KXEMLA33W94Y972E"),
("Eugenia","Macdonald","pellentesque@Praesent.edu","3142832666","Buin","1981-10-27","Oklahoma City","75554","Ap #586-5936 Nisl. Rd.",39,"XRLGML88P50A534K"),
("Henry","Blake","sed.leo.Cras@Suspendisse.co.uk","3464694217","Newport","1981-07-12","Gondiya","36455","Ap #460-9428 Libero Av.",13,"MODPKN65N79A985G"),
("Ginger","Francis","et@variusNam.co.uk","0814161133","Labuissire","1982-08-02","Vielsalm","19685","P.O. Box 978, 8368 Adipiscing Avenue",59,"LJHTPC35D56Z197I"),
("Ulysses","Preston","convallis@ut.co.uk","0817705739","Ripacandida","1986-06-11","Clermont-Ferrand","16681","177 Malesuada Ave",68,"KJRFBP87B92Y991H"),
("Naomi","Gates","blandit@sollicitudin.org","0814805167","Nogales","1972-09-12","Booischot","89440","P.O. Box 651, 2955 Sit St.",40,"HOTHEE68X57P552U"),
("Salvador","Hess","vitae.dolor.Donec@eu.edu","0819322181","Maidenhead","1978-06-27","Milwaukee","37060","P.O. Box 610, 9151 Malesuada Av.",42,"JCARVO42X49C215V"),
("Dalton","Bowman","Aenean@eget.com","0819601993","Fraser-Fort George","1978-04-02","Merritt","14961","6519 Tincidunt, Rd.",69,"VJDHWH66F75H783C"),
("Sasha","Knox","Integer.in@Duissit.net","0817052587","Aliano","1979-05-03","Toltén","30137","P.O. Box 558, 7112 Aliquam Road",33,"BMVFLZ77F76H559O"),
("Cadman","Hammond","elementum.sem.vitae@dictumsapien.edu","3947573919","Saint-Pierre","1989-11-12","Sangli","61456","Ap #265-2784 Per Street",64,"BZLWVZ23U62V604I"),
("Kyla","Walter","facilisis@lobortisquispede.com","0813397571","Merseburg","1986-06-15","Kilwinning","00120","6408 Vestibulum Street",80,"VWQPGA08M20W332V"),
("Blaze","Kirby","sed.sapien.Nunc@Maurisblandit.ca","0813980123","Fort William","1995-08-03","Lisciano Niccone","55245","442-3734 Quam, Street",19,"LBFLYV25M64D617M"),
("Kamal","Cote","In@sedtortorInteger.edu","3017725476","Lac Ste. Anne","1993-11-18","Pelotas","57250","Ap #924-5791 Tellus Rd.",54,"MCUKIS78C34H891G"),
("Austin","Rosa","Nunc@justoProinnon.com","0814910213","Grimsby","1991-10-09","Purulia","68331","231-9978 Interdum St.",29,"NPXRTY89I99S593Z"),
("Jasper","Weaver","molestie.sodales@ultricesposuerecubilia.ca","0813103034","Caxias","1977-05-14","Vellore","21145","128-9966 In Avenue",61,"BMJSGA49F02X879F"),
("Dante","Mckenzie","enim.sit@etlaciniavitae.com","0818539363","Lonzee","1975-12-14","LaSalle","32114","171-4364 Nascetur Ave",89,"BJQJMU26L92S048L"),
("Dillon","Skinner","enim.Mauris@aliquetodioEtiam.edu","3850000278","Recogne","1977-03-03","Qualicum Beach","77760","4443 Ultricies Av.",65,"DWPKWK93C39R416E"),
("MacKenzie","Walker","Nam@ipsum.org","0814441737","Denver","1996-07-12","Warburg","27441","Ap #892-4817 Vivamus St.",19,"CBXNQF10F15O188U"),
("Pascale","Martinez","Nunc.quis@Nullatempor.org","0812140422","Mandasor","1981-12-18","Athus","18388","7437 Nec Av.",75,"DBDXVQ64A00F397L"),
("Abigail","Alston","ipsum.leo@parturientmontes.ca","0819163004","Mold","1972-08-18","Crystal Springs","28663","317-9702 Et, St.",6,"KTCWZN04K98S153X"),
("Gray","Harvey","consequat.enim@Nuncsollicitudin.org","3228978314","Carmarthen","1982-03-10","Frauenkirchen","87282","170-9489 Ridiculus Avenue",18,"WUMIFL08Q54Z182P"),
("Florence","Houston","Nam@etcommodoat.net","3523566682","Cardiff","1975-12-26","Istanbul","45932","6202 Non, Rd.",29,"OTDNXU29X33K416H"),
("Dillon","Lynn","ante.iaculis.nec@sempertellusid.net","3840568005","Missoula","1993-11-23","Rivire","10074","216-3373 Pede Street",74,"ROSTGZ42N99Y907W"),
("Myles","Rush","egestas.Aliquam@vestibulumloremsit.co.uk","0817477336","Macul","1998-07-31","Jennersdorf","90551","780-4631 Iaculis Rd.",98,"MEYATX34P77W229Z"),
("Hammett","Simon","velit@laciniamattis.ca","0811923973","Richmond","1994-09-22","Palermo","84753","P.O. Box 954, 8762 Ipsum Avenue",99,"STYDUS70L01X317F"),
("Kyra","Jarvis","ligula.Donec.luctus@ante.com","0815988989","Lacombe","1988-05-29","San Antonio","79074","Ap #877-3996 Magna St.",62,"NTHKEB66Q89B719M"),
("Cameran","Conley","nec.imperdiet.nec@elitNullafacilisi.com","3165029964","Kozhikode","1997-08-08","Chesapeake","45925","8900 Et Rd.",32,"BHYRQK89C37L058P"),
("Theodore","Mayer","dolor.Donec.fringilla@egestasurnajusto.edu","3793634882","Crowsnest Pass","1978-06-23","Sulzano","49279","143-6570 Magnis Av.",19,"MUSCLF62T92R148Q"),
("Helen","Harrell","neque@eumetus.net","0812822948","Sterling Heights","1973-05-22","Norman Wells","20966","Ap #798-4432 Montes, St.",54,"JPKNTZ29I05H087J"),
("Eleanor","Wise","nascetur.ridiculus.mus@enimnectempus.org","0817193794","Newton Stewart","1984-02-02","Spremberg","94194","Ap #688-469 Scelerisque St.",4,"DUFOEY93A83M880N"),
("Graiden","Dotson","tristique.senectus.et@venenatisvel.org","0811832945","Sant'Egidio alla Vibrata","1995-11-08","Casnate con Bernate","43449","Ap #171-2356 Semper Street",23,"HAXUSI80F23J582X"),
("Thomas","Carlson","Fusce@sapienmolestie.org","0816286241","Monticelli d'Ongina","1985-12-24","Paranaguá","99952","3194 Morbi Avenue",94,"VKHDIE54G72A933I"),
("Echo","Branch","metus@tinciduntnibh.net","3772133739","Sevilla","1995-01-08","Vichte","72960","799-4868 Et St.",42,"UHZKCI89S78S208I"),
("Blake","Grimes","odio.tristique@purusaccumsan.com","0816981020","İnegöl","1993-09-21","Kapolei","60246","Ap #502-8922 Nisl Avenue",61,"CHRVJW50L06U893E"),
("Vanna","Graves","vitae.purus@semvitae.net","0817935004","Colmar","1996-07-03","Enines","37182","Ap #303-7554 Sollicitudin St.",53,"ALIYHC27B12V960N"),
("Ora","Banks","Suspendisse.sed@aliquamiaculis.org","3700534498","Innisfail","1986-06-11","Napier","29039","5551 Vivamus Ave",93,"BZLFHT31F13A993T"),
("Ayanna","Bean","litora@dolornonummyac.org","3356304477","Agra","1988-08-02","Mustafakemalpaşa","78201","P.O. Box 347, 4200 Mauris Avenue",24,"RMNUML07P52E315L"),
("Burke","Porter","sollicitudin.adipiscing@disparturient.org","0818357898","Freital","1979-11-12","Giarratana","83156","294-3550 Urna Ave",5,"QEXMRC68S08X207K"),
("Haviva","Farmer","turpis.In.condimentum@magna.edu","0811011746","Lidköping","1989-08-11","Chaudfontaine","79430","P.O. Box 914, 1370 Nisl. Street",94,"EIKYFM59Z03W572B"),
("Ursa","Cherry","varius.ultrices.mauris@SednequeSed.net","0810869427","Drachten","1983-12-06","Plymouth","98936","155-4518 Felis St.",36,"ZSVMKX98P46P504N"),
("Uriah","Guy","ipsum@hendreritconsectetuer.co.uk","3511668905","York","1983-02-19","Bremen","74117","6001 Elit, St.",58,"XCADJB39A79Y533B"),
("Acton","Hansen","sit@vehiculaPellentesquetincidunt.com","0811325065","La Valle/Wengen","1995-09-26","Saskatoon","44423","Ap #712-580 Vivamus Av.",52,"LDGADX74R30C626R"),
("Wynne","Livingston","Fusce.aliquam@quam.co.uk","3842973035","Dutse","1993-02-03","Owensboro","37388","5181 Urna. Av.",100,"LOZGTX42K43A694K"),
("Rama","Ellison","sit.amet.metus@scelerisquelorem.org","3054730153","Patan","1983-05-13","Campagna","92507","P.O. Box 178, 2210 Nec Rd.",6,"FHILYP56L82E902U"),
("Ursa","Suarez","vel.nisl.Quisque@sit.co.uk","3949456320","Santu Lussurgiu","1974-05-10","Imst","96371","431-9994 Nibh. Avenue",26,"TYJKWH97Y79C129M"),
("Hu","Jensen","augue@dolor.ca","0817081035","Mesa","1998-03-05","Guben","12854","P.O. Box 440, 4220 Quisque Rd.",37,"LCVEOY30A38F846O"),
("Gemma","Beasley","in.lobortis.tellus@egestasDuis.org","0817331240","Ruddervoorde","1993-02-16","Jalandhar (Jullundur)","26400","P.O. Box 438, 4855 Egestas. Rd.",57,"USCMXR25N89Y086A"),
("Quinlan","Rosa","Nunc.sed@ligulaAliquamerat.org","3214420403","Vichuquén","1979-04-24","Cleveland","31906","3350 Dui. Road",18,"EHWVBP70K32M039C"),
("Kuame","Charles","et.magnis.dis@blanditmattis.org","3654489391","Arnesano","1995-10-24","Gresham","90875","9931 Viverra. St.",79,"NVCAVL67S50T095V"),
("Shellie","Conner","eu@turpisIncondimentum.org","3759465021","Onitsha","1981-02-27","Punta Arenas","98828","P.O. Box 703, 7976 Ante Ave",13,"AROARO72Y64E030Y"),
("Holmes","Garrison","interdum@tempuseuligula.com","0813941108","Lozzo Atestino","1995-02-01","Toulouse","12919","Ap #195-7134 Mi St.",12,"FFGUDC73V78Y254Q"),
("Pascale","Ortiz","libero.Proin.sed@eu.net","0818862613","Fort Simpson","1980-07-11","Lewiston","49659","9749 Sed Avenue",29,"XMSGZV51W39H599C"),
("Adam","Keller","Nunc.ut.erat@tempor.net","0816393492","Fleurus","1977-03-02","Buckingham","60486","P.O. Box 958, 6838 Mattis Ave",28,"IRBATB80T51D765N"),
("Gil","Barker","aliquam@Nulla.co.uk","0816989758","Pavone del Mella","1986-01-27","Agen","92157","411-5691 Rutrum Rd.",100,"HMZNND49Z91J223C"),
("Mallory","Farmer","tristique@Integeridmagna.edu","0815881425","Laces/Latsch","1983-10-27","Montleban","75547","P.O. Box 410, 3467 At St.",63,"AQGTYR95T26D473E"),
("Howard","Swanson","odio.auctor@erat.org","3257518734","Madison","1985-08-26","San Rafael","81283","784-8545 Tincidunt Rd.",45,"EGMMVM73I52S708B"),
("Iliana","Dejesus","iaculis.odio@ut.net","0811941081","Kozhikode","1995-02-21","Hoogstraten","73695","9494 Cum Street",11,"EKXAYG89U83A108E"),
("Graham","Barron","a@lobortis.edu","3277746981","Jalandhar (Jullundur)","1989-06-10","Chimay","16171","Ap #120-2089 Montes, Av.",23,"EAKYIQ14G81O270V"),
("Amy","Lopez","Donec@nibhenimgravida.com","0819584587","Amstelveen","1971-05-24","Salisbury","49204","837 Elit, Avenue",15,"PBZXNC98N67K261U"),
("Jacqueline","Zimmerman","Donec@sollicitudin.co.uk","3488158912","Jodoigne","1985-08-24","Buckingham","70919","4279 Est. St.",65,"TSYSXN51J67M465J"),
("Rylee","Lamb","malesuada.fames.ac@vulputate.net","3374442088","Huntley","1972-01-13","Haut-Ittre","10258","9794 Posuere Rd.",49,"EOFRWL45X12O714V"),
("Troy","Tucker","luctus.vulputate.nisi@Phasellusnulla.co.uk","3343239659","Neath","1987-11-12","Copiapó","58426","942-5188 Ut, Av.",97,"ZJAKMM47W34T518R"),
("Laura","Nelson","dui.nec.urna@nibhlaciniaorci.edu","3119622124","Montbéliard","1995-02-18","Manisa","86734","667-5961 Mus. St.",4,"STRDRP48M06T879Y"),
("Hayes","Edwards","sociosqu.ad@acsem.co.uk","0817387475","Mignanego","1992-04-08","San Giovanni in Galdo","36526","P.O. Box 631, 8045 Eleifend St.",76,"QCYOQR20Y04A886S"),
("Lev","Riley","Donec@variusorciin.edu","3175208674","Forio","1976-03-09","High Wycombe","16216","P.O. Box 675, 5872 Gravida. Avenue",16,"EBYZZX40X58A538Z"),
("Judah","Jackson","non.cursus@acrisus.net","3542628011","Acquasanta Terme","1972-09-13","Arauco","22751","P.O. Box 640, 3979 Est. Ave",92,"UWSEHM56T82U568F"),
("Autumn","Briggs","sodales@odioPhasellus.edu","0813607020","Bon Accord","1996-03-20","Traiguén","59651","Ap #330-1195 Sapien. Street",75,"PQPLGX42E05E523C"),
("Harper","Carey","enim@nequeNullamnisl.co.uk","3554504104","Buguma","1978-01-29","Stockport","11190","Ap #966-1399 Tempor, Street",97,"IWSPXE37B27N763I"),
("Omar","Miranda","semper.et@duiCraspellentesque.edu","3880124527","Kermt","1983-09-11","Wellington","12004","1128 Congue St.",42,"AOBXLK20C33W267K"),
("Keith","Hernandez","ornare.tortor.at@fringillaornare.org","3382590323","Midway","1977-10-23","Fossato di Vico","32386","Ap #251-6279 Et Ave",90,"ITGBPZ69A48Y113L"),
("Russell","Payne","eros@Vestibulum.ca","0810368718","Rangiora","1994-11-17","Finkenstein am Faaker See","05037","963 Sagittis Avenue",62,"VVIUFR49V82Q290V"),
("Phoebe","Holland","pede@faucibus.com","0813202411","Goderich","1990-03-16","Banff","55400","466-5742 Nibh Rd.",19,"EYSZKN87R85N045M"),
("Wallace","Wilder","mollis@Donec.ca","0819919730","Lleida","1995-04-14","Sunset Point","65959","704-3172 Tempor Rd.",64,"IWNTKI93H90B865F"),
("September","Walton","non.luctus.sit@consectetuer.co.uk","3365460015","Hudiksvall","1990-07-21","Raipur","72979","P.O. Box 174, 9810 Felis Road",46,"LXMQEO88Q68A780X"),
("Cole","Brennan","Curabitur.vel@diamatpretium.org","3806974123","Götzis","1980-02-20","Galzignano Terme","25721","P.O. Box 508, 2802 Leo. Av.",96,"EJXMJP83Q67U481Z"),
("Yoko","Peck","Aliquam.fringilla@metusIn.ca","3149726923","Cañete","1988-08-19","Sogliano Cavour","61318","P.O. Box 976, 4431 Eleifend. Rd.",29,"HFOYCW09B32T674Q"),
("Jasper","Palmer","a.dui@magnaNamligula.org","0812035193","Grand-Hallet","1992-07-05","Cour-sur-Heure","70387","4776 Leo. St.",30,"WEPONZ78E74L719Z"),
("Davis","Rojas","dictum@Phasellusdapibus.co.uk","0818029326","Habra","1993-01-11","Paredones","45999","P.O. Box 687, 5389 Morbi Avenue",6,"STUTMQ43J37J501I"),
("Florence","James","Donec@diam.co.uk","0816811903","Embourg","1980-10-24","Mal","81575","7213 Risus. Rd.",33,"CTJTLW36P57S469V"),
("Minerva","Houston","ipsum.dolor.sit@Sed.edu","3270037874","Deline","1994-05-14","Cavaion Veronese","85576","4304 In Ave",92,"FPBMXV80F51O574C"),
("Jessica","Kaufman","nonummy@nislelementum.net","0817756103","Torquay","1973-07-03","Heredia","39247","P.O. Box 824, 6203 Eu, Av.",70,"UVCWGI39B49E203J"),
("Fleur","Velazquez","est@molestiesodalesMauris.ca","3170478696","Jamnagar","1972-06-13","Stamford","58583","Ap #387-7718 Dictum Rd.",25,"SMFPIY95I47S415J"),
("Yasir","Talley","vehicula.aliquet.libero@vehicula.org","3095258626","Rosarno","1981-01-20","Wiekevorst","10695","177-4563 Arcu. Avenue",14,"HEZSLP52L88B293Z"),
("Reed","Craft","volutpat@risusquisdiam.ca","3551263746","Rigolet","1984-02-06","Rae Bareli","48691","P.O. Box 491, 7611 Nunc Rd.",52,"TUNOBC49H04M456V"),
("Hall","Vinson","Duis.at@magnaSed.co.uk","0816029913","Rimouski","1998-04-18","Bhusawal","73564","9907 Vestibulum Street",95,"EKUFPW11R66D983F"),
("Britanney","Hatfield","ornare.In.faucibus@velarcueu.org","0819279823","Santo Stefano Quisquina","1986-08-23","Calvello","55158","2674 Mollis. St.",28,"GPCGRK64R43X359H"),
("Wallace","Mcbride","rhoncus.Nullam@idante.net","3110581509","Heppenheim","1981-10-23","Moio Alcantara","51684","P.O. Box 123, 9534 Sed St.",26,"WQFLVT76J35G403D"),
("Camden","Zamora","cursus.et@aliquamadipiscinglacus.co.uk","3297797455","Schwedt","1981-02-09","Lede","52293","9239 Ornare Rd.",93,"SWPLWK26V35J672V"),
("Richard","Burgess","non.ante@Infaucibus.co.uk","3953454430","Dampremy","1975-02-16","Midway","14753","445-6438 Ornare, Avenue",62,"CFTOIT79J43P593L"),
("Steven","Gardner","dolor.sit.amet@nec.edu","0811658424","Ficarolo","1990-11-21","María Pinto","80596","669-7892 Nam Street",59,"WPEPSQ17T80Q138K"),
("Ifeoma","Valentine","senectus.et@Suspendissealiquetsem.org","0811072936","Bismil","1974-11-09","Ophoven","21995","P.O. Box 272, 4031 At Road",87,"ZARMOU41Y82A631C"),
("Bethany","Gordon","semper.Nam.tempor@ultrices.org","0811678708","Lampeter","1980-11-18","Bierk Bierghes","88809","Ap #583-6011 Dis Ave",38,"TRWQWR32V14X643Z"),
("Orlando","Mendez","sagittis@Loremipsum.org","0810758586","Cerignola","1980-09-10","Sittard","70700","Ap #887-2141 Odio, Av.",37,"HKSUCR79F38D972X"),
("Jemima","Maynard","ut@pellentesqueegetdictum.net","0810562994","Saarbrücken","1989-07-24","Glovertown","18174","P.O. Box 938, 7696 Cum Rd.",59,"OGTPID47Q68Y681X"),
("MacKenzie","Richardson","tellus@enimNunc.net","3256062464","Port Moody","1976-01-12","Dampicourt","26273","4409 Gravida. Ave",87,"UKWQUK74U43M297U"),
("Moana","Dillon","eget.volutpat@liberoProinsed.edu","3729028594","Alix","1971-09-16","Camiña","55874","Ap #342-5342 Eget Rd.",88,"VRKOWV39S34S775E"),
("August","Gaines","accumsan@Vivamus.edu","3593711152","Eckville","1980-01-26","Hualpén","69047","734-4535 Euismod St.",53,"NSVSWH37E89W530E");



CREATE TABLE `PaymentOrder` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  `id_c` mediumint default NULL REFERENCES CLIENT(ID),
  `date_start` varchar(255),
  `amount` varchar(100) default NULL,
  `status` varchar(255) default NULL
);

INSERT INTO `PaymentOrder` (`id_c`,`date_start`,`amount`,`status`) VALUES (63,"2006-04-05","208,58","Paid"),
(100,"2007-07-12","204,32","NotSent"),
(69,"2009-02-24","475,14","Urgent"),
(77,"2007-05-27","103,16","Paid"),
(22,"2008-05-22","412,96","Paid"),
(22,"2011-07-20","545,88","Contested"),
(22,"2010-03-02","359,13","Paid"),
(39,"2016-01-22","591,54","NotSent"),
(78,"2015-07-11","773,52","Contested"),
(83,"2008-07-13","449,40","Sent"),
(1,"2009-08-21","152,07","Paid"),
(41,"2016-07-17","284,55","Sent"),
(75,"2010-10-11","76,80","NotSent"),
(7,"2012-02-09","393,98","Paid"),
(9,"2006-06-10","411,45","NotSent"),
(87,"2007-05-30","641,43","Paid"),
(53,"2011-01-05","602,49","Contested"),
(11,"2013-04-01","323,47","Contested"),
(14,"2014-03-22","440,41","Sent"),
(8,"2014-02-27","467,30","Sent"),
(68,"2009-02-01","733,38","Urgent"),
(82,"2007-04-30","462,01","NotSent"),
(46,"2014-03-19","114,94","Urgent"),
(84,"2010-10-26","787,01","Urgent"),
(5,"2010-04-15","227,96","Sent"),
(88,"2012-12-03","249,57","Urgent"),
(36,"2007-05-22","955,09","Urgent"),
(78,"2014-02-21","406,61","Contested"),
(96,"2016-06-22","300,37","Contested"),
(24,"2008-02-29","366,56","Contested"),
(11,"2008-07-21","682,54","Urgent"),
(80,"2007-01-10","826,63","NotSent"),
(95,"2007-01-26","343,39","Urgent"),
(32,"2014-06-29","262,84","Paid"),
(10,"2016-08-23","847,71","Contested"),
(82,"2006-02-27","405,43","Paid"),
(14,"2011-01-08","552,52","Paid"),
(52,"2013-08-30","917,78","Sent"),
(23,"2012-08-26","511,90","Urgent"),
(70,"2008-04-09","725,52","Sent"),
(44,"2008-01-05","765,39","Paid"),
(46,"2005-09-01","66,64","Contested"),
(15,"2007-08-26","473,54","Urgent"),
(90,"2016-07-22","96,10","NotSent"),
(21,"2016-07-14","845,15","Sent"),
(41,"2009-07-07","656,76","Paid"),
(88,"2015-10-02","810,88","NotSent"),
(33,"2006-03-10","474,75","Urgent"),
(74,"2015-12-17","928,11","Urgent"),
(42,"2010-04-28","105,52","NotSent"),
(98,"2010-05-15","201,15","Sent"),
(82,"2011-02-10","863,09","Contested"),
(28,"2007-05-17","433,38","Urgent"),
(22,"2008-12-28","60,85","Sent"),
(80,"2015-08-23","411,08","Urgent"),
(20,"2006-12-29","838,29","Sent"),
(80,"2006-11-10","353,61","Urgent"),
(80,"2009-09-25","704,75","Paid"),
(77,"2010-03-17","717,76","Sent"),
(2,"2008-04-09","577,66","Sent"),
(60,"2013-12-28","369,53","Sent"),
(51,"2015-09-09","815,46","Contested"),
(63,"2008-02-04","116,55","Contested"),
(75,"2014-01-04","848,53","Sent"),
(12,"2008-07-21","172,96","Sent"),
(94,"2010-01-02","461,53","NotSent"),
(45,"2009-01-21","613,22","Urgent"),
(52,"2006-01-31","848,10","Contested"),
(57,"2014-01-25","723,68","Paid"),
(16,"2010-12-01","687,99","Sent"),
(40,"2012-04-03","746,83","Sent"),
(31,"2010-08-01","824,91","Contested"),
(61,"2007-06-28","483,45","Paid"),
(80,"2015-01-17","803,63","NotSent"),
(72,"2011-05-30","789,42","Paid"),
(92,"2012-10-24","351,58","NotSent"),
(86,"2008-07-11","685,58","Paid"),
(56,"2008-12-10","351,07","NotSent"),
(93,"2013-09-21","167,29","Urgent"),
(95,"2012-11-15","780,70","Paid"),
(28,"2016-08-23","79,36","NotSent"),
(54,"2007-05-25","936,32","Sent"),
(90,"2012-04-20","973,64","Contested"),
(76,"2012-12-05","613,66","Urgent"),
(88,"2015-04-03","867,19","Paid"),
(60,"2006-10-14","803,94","Paid"),
(49,"2011-08-03","943,16","Sent"),
(71,"2014-09-25","588,99","Contested"),
(36,"2014-04-27","722,23","Contested"),
(93,"2010-03-27","910,77","Sent"),
(80,"2012-06-12","706,62","Contested"),
(44,"2015-08-13","191,79","Urgent"),
(7,"2008-06-05","222,60","NotSent"),
(7,"2008-07-02","621,91","Urgent"),
(79,"2006-10-30","146,57","NotSent"),
(16,"2012-01-02","446,18","NotSent"),
(41,"2012-07-08","937,89","Urgent"),
(70,"2015-08-07","540,63","NotSent"),
(50,"2009-09-11","421,25","Paid"),
(51,"2011-07-21","233,52","Contested");



CREATE TABLE Credential (

'username' varchar(30) PRIMARY KEY NOT NULL,
'password' varchar(30) NOT NULL

);

INSERT INTO 'Credential' (username, password) VALUES 
("Stefano","Carannante"),
("Gennaro","Coppola"),
("Marco","12345"),
("Franco","Napoli"),
("Anna","Cane");
