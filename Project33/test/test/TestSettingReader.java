package test;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import extra.SettingReader;

/**
 *
 * @author team33
 */
public class TestSettingReader {
    @Before
    public void createFile(){
        try {
            FileWriter f = new FileWriter("fileTest", false);
            f.write("NOME=TESTER;\n");
            f.write("LUOGO=NAPOLI;\n");
            f.write("IP=127.0.0.1;\n");
            f.write("FILE=abc.db;\n");
            f.write("SUCCESS=TRUE;\n");
            f.close();
            
            FileWriter b = new FileWriter("fileTest2", false);
            b.write("\n");
            b.close();
            
        } catch (IOException ex) {
            Logger.getLogger(TestSettingReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @After
    public void deleteFile(){
        new File("fileTest").delete();
        new File("fileTest2").delete();
    }
    
    @Test
    public void testCorrectValues(){
        SettingReader sr = new SettingReader("fileTest");
        HashMap<String, String> settings = sr.getSettings();
        
        assertEquals("TESTER",settings.get("NOME"));
        assertEquals("NAPOLI",settings.get("LUOGO"));
        assertEquals("127.0.0.1",settings.get("IP"));
        assertEquals("abc.db",settings.get("FILE"));
        assertEquals("TRUE",settings.get("SUCCESS"));
    }
    
    @Test
    public void testNumberOfKeys(){
        SettingReader sr = new SettingReader("fileTest");
        HashMap<String, String> settings = sr.getSettings();
        assertEquals(5, settings.size());
    }
    
    @Test
    public void testEmptyFile(){
        SettingReader sr = new SettingReader("fileTest2");
        HashMap<String, String> settings = sr.getSettings();
        assertEquals(0, settings.size());
    }
}
