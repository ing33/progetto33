package test;

import org.junit.*;
import static org.junit.Assert.*;
import controller.DaoClient;
import entity.Client;
import java.util.ArrayList;

/**
 * @author team33
 */
public class TestDaoClient {
    
    @Test
    public void testGetByID(){
        DaoClient dc = new DaoClient();
        Client cl = dc.getById(1);
        System.out.println(cl.toString());
        assertEquals("Giacomo", cl.getName());
        assertEquals("Moody", cl.getSurname());
        assertEquals("0812138039", cl.getPhone());
        assertEquals("1986-06-02", cl.getDate_birth());
    }

    
    @Test
    public void testUpdate(){
        DaoClient dc = new DaoClient();
        Client cl = dc.getById(1);
        assertEquals("Fresia", cl.getCity_birth());
        cl.setCity_birth("Rome");
        dc.update(cl);
        cl = dc.getById(1);
        assertEquals("Rome", cl.getCity_birth());
        
        cl.setCity_birth("Fresia");
        dc.update(cl);
    }
    
    
    @Test
    public void testSearch(){
        DaoClient dc = new DaoClient();
        ArrayList<Client> vet = dc.search("Camden Zamora");
        assertEquals("Lede", vet.get(0).getCity_residence());
        vet = dc.getAll();
        assertEquals(100,vet.size());
    }
    
    
    @Test
    public void testDelete(){
        /* Test Delete è vuoto perchè cancellando un cliente non posso ricrearne
        un altro con lo stesso id e quindi altera anche gli altri test.
        Semplicemente dovremmo avere uno script che rigenera il database ogni
        volta che vogliamo eseguire i test, ma questo non è necessario per l'esame.
        */
    }
}