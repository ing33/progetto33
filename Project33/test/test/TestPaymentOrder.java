package test;

import org.junit.*;
import static org.junit.Assert.*;
import controller.DaoPaymentOrder;
import entity.PaymentOrder;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author team33
 */
public class TestPaymentOrder {
    
    @Test
    public void testGetByID(){
        DaoPaymentOrder dpo = new DaoPaymentOrder();
        PaymentOrder po = dpo.getById(1);
        assertEquals(208,58, po.getAmount());
        assertEquals("Paid", po.getStatus());
    }

    
    @Test
    public void testUpdate(){
        DaoPaymentOrder dpo = new DaoPaymentOrder();
        PaymentOrder po = dpo.getById(1);
        assertEquals("Paid", po.getStatus() );
        po.setStatus("Urgent");
        dpo.update(po);
        po = dpo.getById(1);
        assertEquals("Urgent", po.getStatus());
        
        po.setStatus("Paid");
        dpo.update(po);
    }

    
    @Test
    public void testSearch(){
        DaoPaymentOrder dpo = new DaoPaymentOrder();
        ArrayList<HashMap<String,String>> res = dpo.search("Jasper");
        assertEquals(6, res.size());
        res = dpo.search("Jasper 2007");
        assertEquals(1, res.size());
        assertEquals("NotSent", res.get(0).get("STATUS"));
    }


    @Test
    public void testDelete(){
        /* Test Delete è vuoto perchè cancellando un cliente non posso ricrearne
        un altro con lo stesso id e quindi altera anche gli altri test.
        Semplicemente dovremmo avere uno script che rigenera il database ogni
        volta che vogliamo eseguire i test, ma questo non è necessario per l'esame.
        */
    }
}