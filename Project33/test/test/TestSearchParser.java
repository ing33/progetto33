package test;

import static org.junit.Assert.*;
import org.junit.Test;
import extra.SearchParser;

/**
 *
 * @author team33
 */
public class TestSearchParser {
    @Test
    public void testYear(){
        /* Correct case */
        SearchParser sp1 = new SearchParser("Tester 2011 Beta");
        assertEquals(2011, sp1.getYear());
        /* There is no year => year should be -1 */
        SearchParser sp2 = new SearchParser("Tester Beta");
        assertEquals(-1, sp2.getYear());
        /* There is a number but it's not in the range [1990:2100] */
        SearchParser sp3 = new SearchParser("Testing Text text 183 xD");
        assertEquals(-1, sp3.getYear());
        /* It tests that it doesn't matter the position of the year */
        SearchParser sp4 = new SearchParser("2015 Pippo pluto");
        assertEquals(2015, sp4.getYear());
    }
    
    @Test
    public void testValues(){
        /* Normal case */
        SearchParser sp1 = new SearchParser("Testing Dev Junit Great INGSW Wow");
        assertEquals(6, sp1.getValues().size());
        assertEquals("Testing" ,sp1.getValues().get(0));
        assertEquals("Dev" ,sp1.getValues().get(1));
        assertEquals("Junit" ,sp1.getValues().get(2));
        assertEquals("Great" ,sp1.getValues().get(3));
        assertEquals("INGSW" ,sp1.getValues().get(4));
        assertEquals("Wow" ,sp1.getValues().get(5));
        
        /* Check that also when there is a year, the final array is correct */
        SearchParser sp2 = new SearchParser("Testing Dev Junit Great 2010 INGSW Wow");
        assertEquals(6, sp2.getValues().size());
        assertEquals("Testing" ,sp2.getValues().get(0));
        assertEquals("Dev" ,sp2.getValues().get(1));
        assertEquals("Junit" ,sp2.getValues().get(2));
        assertEquals("Great" ,sp2.getValues().get(3));
        assertEquals("INGSW" ,sp2.getValues().get(4));
        assertEquals("Wow" ,sp2.getValues().get(5));
    }
    
    @Test
    public void testComposeValues(){
        SearchParser sp = new SearchParser("name1 surname1 CF");
        assertEquals("name='name1' OR name='surname1' OR name='CF' OR"
                + " surname='name1' OR surname='surname1' OR surname='CF'"
                + " OR CF='name1' OR CF='surname1' OR CF='CF' " ,
                sp.composeValue(new String[]{"name","surname","CF"}));
        
        SearchParser sp2 = new SearchParser("surname1");
        assertEquals("surname='surname1' ",sp2.composeValue(new String[]{"surname"}));
    }
    
    @Test
    public void testComposeDate(){
        SearchParser sp = new SearchParser("name surname 2009");
        assertEquals("( date_birth > '2009-01-01' AND date_birth < '2009-12-31' )", sp.composeDate("date_birth"));
    }
}
