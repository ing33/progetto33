package test;

import org.junit.*;
import static org.junit.Assert.*;
import boundary.Login;
import controller.LoginController;

/**
 *
 * @author team33
 */
public class TestLoginController {
    Login lg = new Login();
    
    @Test
    public void test1(){
        LoginController lgc = new LoginController(lg);
        lgc.verifyLogin("antonio", "ciao");
        assertEquals(0, lgc.getSuccess());
    }
    
    @Test
    public void test2(){
        LoginController lgc2 = new LoginController(lg);
        lgc2.verifyLogin("Gennaro", "Coppola");
        assertEquals(1, lgc2.getSuccess());
    }
}
