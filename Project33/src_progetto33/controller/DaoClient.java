package controller;

import entity.Client;
import extra.SearchParser;
import extra.SettingReader;
import java.util.ArrayList;
import java.sql.*;
import java.util.HashMap;

/**
 *
 * @author team33
 */
public class DaoClient {
    
    private Connection conn = null;
    private Statement stmt = null;
    
    public DaoClient(){
        HashMap<String, String> settings = new SettingReader("config.ini").getSettings();
        try{
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:"+settings.get("DB"));
        } catch ( ClassNotFoundException | SQLException e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
    }
    
    public void create(){
        try{
            stmt = conn.createStatement();
            String command = "INSERT INTO CLIENT (id) VALUES(null);";
            stmt.execute(command);
            stmt.close();
        } catch (SQLException e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }
    
    public void delete(int id){
        try{
            stmt = conn.createStatement();
            String command = "DELETE FROM CLIENT WHERE ID=" + id;
            stmt.execute(command);
            stmt.close();
        } catch (SQLException e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }
    
    public void update(Client updated){
        try{
            stmt = conn.createStatement();

            String command = "UPDATE CLIENT SET "
                    + "NAME='"+ updated.getName()+ "', "
                    + "SURNAME='"+ updated.getSurname()+ "', "
                    + "EMAIL='"+ updated.getEmail()+ "', "
                    + "PHONE='"+ updated.getPhone()+ "', "
                    + "DATE_BIRTH='"+ updated.getDate_birth()+ "', "
                    + "CITY_BIRTH='"+ updated.getCity_birth()+ "', "
                    + "CITY_RESIDENCE='"+ updated.getCity_residence()+ "', "
                    + "STREET='"+ updated.getStreet()+ "', "
                    + "ZIP='"+ updated.getZip()+ "', "
                    + "CIVIC_NUMBER='"+ updated.getCivic_number()+ "', "
                    + "CF='"+ updated.getCF()+ "' "
                    + "WHERE ID="+ updated.getID()+ ";";
            stmt.execute(command);
            stmt.close();
        } catch (SQLException e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }
    
    public ArrayList<Client> search(String parameters){ 
        ResultSet rs;
        SearchParser sp = new SearchParser(parameters);
        String[] cols = {"name","surname","cf","email","city_residence"};
        Client temp;
        ArrayList<Client> Clients = new ArrayList<>();
        try{
            stmt = conn.createStatement();
            String command;
            if(parameters.equals("*") || parameters.equals("")){
                command = "SELECT * FROM CLIENT";
            } else { 
                command = "SELECT * FROM CLIENT WHERE " + sp.composeValue(cols); 
            }
            rs = stmt.executeQuery(command);
            while( rs.next() ){
                temp = new Client();
                temp.setID(rs.getInt("ID"));
                temp.setName(rs.getString("NAME"));
                temp.setSurname(rs.getString("SURNAME"));
                temp.setEmail(rs.getString("EMAIL"));
                temp.setPhone(rs.getString("PHONE"));
                temp.setCity_birth(rs.getString("CITY_BIRTH"));
                temp.setDate_birth(rs.getString("DATE_BIRTH"));
                temp.setCity_residence(rs.getString("CITY_RESIDENCE"));
                temp.setStreet(rs.getString("STREET"));
                temp.setZip(rs.getString("ZIP"));
                temp.setCivic_number(rs.getString("CIVIC_NUMBER"));
                temp.setCF(rs.getString("CF"));
                
                Clients.add(temp);
            }
            stmt.close();
        } catch (SQLException e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        return Clients;
    }
    
    public Client getById(int id){
        ResultSet rs;
        Client out = null;
        try{
            stmt = conn.createStatement();
            String command = "SELECT * FROM CLIENT WHERE ID=" + id;
            rs = stmt.executeQuery(command);
            if( rs.next() ){
                out = new Client();
                out.setID(rs.getInt("ID"));
                out.setName(rs.getString("NAME"));
                out.setSurname(rs.getString("SURNAME"));
                out.setEmail(rs.getString("EMAIL"));
                out.setPhone(rs.getString("PHONE"));
                out.setCity_birth(rs.getString("CITY_BIRTH"));
                out.setDate_birth(rs.getString("DATE_BIRTH"));
                out.setCity_residence(rs.getString("CITY_RESIDENCE"));
                out.setStreet(rs.getString("STREET"));
                out.setZip(rs.getString("ZIP"));
                out.setCivic_number(rs.getString("CIVIC_NUMBER"));
                out.setCF(rs.getString("CF"));
            }
            stmt.close();
        } catch (SQLException e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        return out;
    }
    public ArrayList<Client> getAll() {
        return search("*");
    }
}
