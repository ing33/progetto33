package extra;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author team33
 *
 * SettingReader class provides a easy way to read from a config file with
 * useful data for an application giving to the application more customizable
 * properties. Correct syntax of these files is: KEY=VALUE;\n KEY=VALUE;\n
 * KEY=VALUE;\n
 *
 * WITHOUT empty spaces
 *
 */
public class SettingReader {

    private HashMap<String, String> settings;
    private String filePath;

    public SettingReader(String filePath) {
        settings = new HashMap();
        try {
            BufferedReader f = new BufferedReader(new FileReader(filePath));
            String line;
            int index_equal, end_sign;
            while ((line = f.readLine()) != null) {
                index_equal = line.indexOf('=');
                end_sign = line.indexOf(';');
                if (index_equal != -1 & end_sign != -1) {  //Necessary control when the file is empty
                    settings.put(
                            line.substring(0, index_equal), /* Key */
                            line.substring(index_equal + 1, end_sign) /* Value */
                    );
                }
            }
        } catch (FileNotFoundException f) {
            System.out.println("Not Found File: " + filePath);
            System.out.println(f.getMessage());
        } catch (IOException ex) {
            Logger.getLogger(SettingReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public HashMap<String, String> getSettings() {
        return settings;
    }

}
