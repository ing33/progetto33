package extra;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author team33
 * SearchParser parses a String and separate the different strings in an array
 * and the optional year value in a integer variable
 */
public class SearchParser {
    ArrayList<String> values;
    private int year;
    public SearchParser(String input){
        year = -1;
        values = new ArrayList(Arrays.asList(input.split(" ")));
        int i = 0, indexRemove = -1;
        for(String s:values){
            try{
                i++;
                this.setYear(Integer.parseInt(s));
                indexRemove = i;
            }
            catch(NumberFormatException e){ }
        }
        if(indexRemove != -1) values.remove(indexRemove-1);
    }
    
    public String composeValue(String[] cols){
        StringBuilder str = new StringBuilder();
        for(String x:cols){
            for(String y:values){
                str.append(x + "='" + y + "' OR ");
            }
        }
        return str.substring(0, str.length()-3);
    }
    
    public String composeDate(String date_name){
        String info = String.valueOf(this.getYear());
        return "( "+ date_name + " > '"+info+"-01-01' AND "+date_name+" < '"+info+"-12-31' )";
    }
    
    public int getYear() {
        return year;
    }

    private void setYear(int year) {
        if( year > 1990 & year < 2100) {
            this.year = year;
        }
    }
    
    public ArrayList<String> getValues() {
        return values;
    }
}
