package exception;

/**
 *
 * @author team33
 */
public class UserNotFoundException extends Exception{

    public UserNotFoundException() {
        super("User not found exception");
    }
    
}
