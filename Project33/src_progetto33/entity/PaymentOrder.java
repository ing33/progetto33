package entity;

/**
 *
 * @author team33
 */
public class PaymentOrder {

    public PaymentOrder(int id_c, double amount,String start_date,String status) {
        this.id_c = id_c;
        this.amount = amount;
        this.start_date = start_date;
        this.status = status;
    }
    
    public PaymentOrder(){}
    
    private int id;
    private int id_c;
    private double amount;
    private String start_date;
    private String status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getId_c() {
        return id_c;
    }
    
    public void setId_c(int c) {
        this.id_c = c;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
