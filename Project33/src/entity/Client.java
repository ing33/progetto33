package entity;

import java.util.Date;
import java.util.Vector;

/**
 *
 * @author team33
 */

public class Client {
    private int ID;
    private String name;
    private String surname;
    private String city_birth;
    private String city_residence;
    private String date_birth;
    private String street;
    private String civic_number;
    private String zip;
    private String email;
    private String phone;
    private String CF;

    public Client(int id, String name, String surname, String city_birth, String city_residence, String date_birth, String street, String civic_number, String zip, String email, String phone, String CF) {
        this.ID = id;
        this.name = name;
        this.surname = surname;
        this.city_birth = city_birth;
        this.city_residence = city_residence;
        this.date_birth = date_birth;
        this.street = street;
        this.civic_number = civic_number;
        this.zip = zip;
        this.email = email;
        this.phone = phone;
        this.CF = CF;
    }
    
    public Client(){ }
    
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCity_birth() {
        return city_birth;
    }

    public void setCity_birth(String city_birth) {
        this.city_birth = city_birth;
    }

    public String getCity_residence() {
        return city_residence;
    }

    public void setCity_residence(String city_residence) {
        this.city_residence = city_residence;
    }

    public String getDate_birth() {
        return date_birth;
    }

    public void setDate_birth(String date_birth) {
        this.date_birth = date_birth;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCivic_number() {
        return civic_number;
    }

    public void setCivic_number(String civic_number) {
        this.civic_number = civic_number;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCF() {
        return CF;
    }

    public void setCF(String CF) {
        this.CF = CF;
    }

    @Override
    public String toString() {
        return "Client{" + "ID=" + ID + ", name=" + name + ", surname=" + surname + ", city_birth=" + city_birth + ", city_residence=" + city_residence + ", date_birth=" + date_birth + ", street=" + street + ", civic_number=" + civic_number + ", zip=" + zip + ", email=" + email + ", phone=" + phone + ", CF=" + CF + '}';
    }

    public Object[] toVector() {
        Object values[] = new Object[6];
        values[0] = ID;
        values[1] = name;
        values[2] = surname;
        values[3] = city_residence;
        values[4] = CF;
        values[5] = date_birth;
        
        return values;
    }
}