package controller;

import boundary.MainWindow;
import boundary.ModifyPODialog;
import entity.PaymentOrder;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author team33
 */

public class POController {
    
    String last_search = "*";
    int selectedRow=0;
    private final MainWindow mw;
    private final DaoPaymentOrder dpo;
    private final GeneratePDFController gpdf;
    
    public POController(MainWindow mw){
        this.mw = mw;
        this.dpo = new DaoPaymentOrder();
        this.gpdf = new GeneratePDFController();
    }
    

    public void delete(){
        dpo.delete(Integer.parseInt((String)mw.poTable.getValueAt(selectedRow, 0)));
        search(last_search);
    }
    
    
    public void search(String parameter){
        last_search = parameter;
        ArrayList<HashMap<String,String>> res = dpo.search(parameter);
        DefaultTableModel dtm = (DefaultTableModel) mw.poTable.getModel();
        
        while(dtm.getRowCount() > 0){
            dtm.removeRow(0);
        }
        
        for(int i=0; i<res.size(); i++){
            dtm.addRow(extractObject(res.get(i)));
        }
    }

    
    public void update(int id, double amount, String status){
        PaymentOrder p = dpo.getById(id);
        p.setAmount(amount);
        p.setStatus(status);
        dpo.update(p);
        search(last_search);
    }
    
    
    public void openModifyDialog(){
        PaymentOrder to_update = dpo.getById(Integer.parseInt((String)mw.poTable.getValueAt(selectedRow, 0)));
        new ModifyPODialog(
                this,
                to_update.getId(),
                to_update.getAmount(),
                to_update.getStatus()
                    ).setVisible(true);
    }
    

    public void changeSelection() {
        selectedRow = mw.poTable.getSelectedRow();
        if(selectedRow == -1) selectedRow = 0;
        PaymentOrder selected = dpo.getById(Integer.parseInt((String)mw.poTable.getValueAt(selectedRow, 0)));
        if(selected.getStatus().equals("Sent")){
            mw.deletePOBtn.setEnabled(false);
        } else {
            mw.deletePOBtn.setEnabled(true);
        }
    }

    public void generatePDF() {
        int id = Integer.parseInt((String)mw.poTable.getValueAt(selectedRow, 0));
        gpdf.generatePDF( id );
        PaymentOrder generated = dpo.getById(id);
        if(generated.getStatus().equals("NotSent")){
            generated.setStatus("Sent");
            dpo.update(generated);
            search(last_search);
        }
    }

    public String getLast() {
        return last_search;
    }
    
    private Object[] extractObject(HashMap<String, String> v){
        Object obj[] = new Object[6];
        obj[0] = v.get("ID");
        obj[1] = v.get("NAME");
        obj[2] = v.get("SURNAME");
        obj[3] = v.get("DATE_START");
        obj[4] = v.get("AMOUNT");
        obj[5] = v.get("STATUS");
        
        return obj;
    }
    
}