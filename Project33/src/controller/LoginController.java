package controller;

import java.sql.*;
import boundary.Login;
import boundary.MainWindow;
import exception.UserNotFoundException;
import extra.SettingReader;

/**
 *
 * @author team33
 */
public class LoginController {

    Login lg;
    int success = 0;
    Connection c = null;
    Statement stmt = null;

    public LoginController(Login lg) {
        SettingReader sr = new SettingReader("config.ini");
        this.lg = lg;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + sr.getSettings().get("DB"));
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    public int getSuccess() {
        return success;
    }

    public void verifyLogin(String user, String password) {
        success = 0;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Credential WHERE username='"
                    + user + "' AND password='"
                    + password + "'");

            if (rs.next()) {
                this.lg.dispose();
                this.success = 1;
                java.awt.EventQueue.invokeLater(() -> {
                    new MainWindow().setVisible(true);
                });
            } else {
                throw new UserNotFoundException();
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        } catch (UserNotFoundException e) {
            this.lg.error_highlight();
            this.success = 0;
        }
    }
}
