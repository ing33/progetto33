package controller;

import entity.Client;
import boundary.MainWindow;
import java.awt.Color;
import java.util.ArrayList;
import java.util.regex.Pattern;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author team33
 */
public class ClientController {

    int selectedRow = 0;
    private final DaoClient dc;
    private final MainWindow mw;
    private ArrayList<Client> vet;

    public ClientController(MainWindow mainw) {
        this.mw = mainw;
        this.dc = new DaoClient();
    }

    public void add() {
        dc.create();
        search("*");
    }

    public void delete() {
        dc.delete((int) mw.clientTable.getValueAt(selectedRow, 0));
        search("*");
    }

    public void update() {
        JTable t = mw.clientTable;
        int id = Integer.parseInt(mw.idLbl.getText());
        Client selected = dc.getById(id);
        Pattern pEmail = Pattern.compile(".*@.*\\.[a-z]{2,3}");
        Pattern pDate = Pattern.compile("[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])");
        Pattern pCF = Pattern.compile("[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z]");
        int error = 0;

        if (!pEmail.matcher(mw.emailTxt.getText()).matches()) {
            mw.emailTxt.setBackground(Color.red);
            error = 1;
        }

        if (!pDate.matcher(mw.dateTxt.getText()).matches()) {
            mw.dateTxt.setBackground(Color.red);
            error = 1;
        }

        if (!pCF.matcher(mw.CFTxt.getText()).matches()) {
            mw.CFTxt.setBackground(Color.red);
            error = 1;
        }

        if (error == 0) {

            selected.setName(mw.nameTxt.getText());
            selected.setSurname(mw.surnameTxt.getText());
            selected.setEmail(mw.emailTxt.getText());
            selected.setPhone(mw.phoneTxt.getText());
            selected.setCity_birth(mw.cityBornTxt.getText());
            selected.setDate_birth(mw.dateTxt.getText());
            selected.setCity_residence(mw.cityResidenceTxt.getText());
            selected.setStreet(mw.streetTxt.getText());
            selected.setCivic_number(mw.civicNumberTxt.getText());
            selected.setCF(mw.CFTxt.getText());

            mw.CFTxt.setBackground(Color.white);
            mw.dateTxt.setBackground(Color.white);
            mw.emailTxt.setBackground(Color.white);
            dc.update(selected);
            search("*");
        }
    }

    public void search(String parameter) {
        ArrayList<Client> res = dc.search(parameter);
        DefaultTableModel dtm = (DefaultTableModel) mw.clientTable.getModel();
        // clean table
        while (dtm.getRowCount() > 0) {
            dtm.removeRow(0);
        }

        for (int i = 0; i < res.size(); i++) {
            dtm.addRow(res.get(i).toVector());
        }
        vet = res;
    }

    public void changedSelection() {
        selectedRow = mw.clientTable.getSelectedRow();
        if (selectedRow == -1) {
            selectedRow = 0;
        }
        int id = Integer.parseInt(mw.clientTable.getValueAt(selectedRow, 0).toString());
        Client selected = dc.getById(id);
        mw.idLbl.setText(String.valueOf(selected.getID()));
        mw.nameTxt.setText(selected.getName());
        mw.surnameTxt.setText(selected.getSurname());
        mw.emailTxt.setText(selected.getEmail());
        mw.phoneTxt.setText(selected.getPhone());
        mw.cityBornTxt.setText(selected.getCity_birth());
        mw.dateTxt.setText(selected.getDate_birth());
        mw.cityResidenceTxt.setText(selected.getCity_residence());
        mw.streetTxt.setText(selected.getStreet());
        mw.civicNumberTxt.setText(selected.getCivic_number());
        mw.CFTxt.setText(selected.getCF());
    }

}
