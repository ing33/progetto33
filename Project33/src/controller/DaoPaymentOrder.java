package controller;

import entity.PaymentOrder;
import extra.SearchParser;
import extra.SettingReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author team33
 */
public class DaoPaymentOrder {

    private Connection conn = null;
    private Statement stmt = null;

    public DaoPaymentOrder() {
        HashMap<String, String> settings = new SettingReader("config.ini").getSettings();
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + settings.get("DB"));
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    public void delete(int id) {
        try {
            stmt = conn.createStatement();
            String command = "DELETE FROM PAYMENTORDER WHERE ID=" + id + ";";
            stmt.execute(command);
            stmt.close();
        } catch (SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public void update(PaymentOrder updated) {
        try {
            stmt = conn.createStatement();
            String command = "UPDATE PAYMENTORDER SET "
                    + "AMOUNT=" + updated.getAmount() + ", "
                    + "STATUS='" + updated.getStatus() + "' "
                    + "WHERE ID=" + updated.getId();
            stmt.execute(command);
            stmt.close();
        } catch (SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public ArrayList<HashMap<String, String>> search(String parameters) {
        ResultSet rs;
        SearchParser sp = new SearchParser(parameters);
        String[] cols = {"name", "surname", "status"};
        ArrayList<HashMap<String, String>> Pos = new ArrayList<>();
        try {
            stmt = conn.createStatement();
            StringBuilder command = new StringBuilder();
            if (parameters.equals("*") || parameters.equals("")) {
                command.append("SELECT PAYMENTORDER.ID, NAME, SURNAME, DATE_START, AMOUNT, STATUS FROM PAYMENTORDER,CLIENT WHERE CLIENT.ID = ID_C");
            } else {
                command.append("SELECT PAYMENTORDER.ID, NAME, SURNAME, DATE_START, AMOUNT, STATUS FROM PAYMENTORDER,CLIENT WHERE CLIENT.ID=ID_C AND (").append(sp.composeValue(cols)).append(")");
                if (sp.getYear() != -1) {
                    command.append(" AND ").append(sp.composeDate("date_start"));
                }
            }
            rs = stmt.executeQuery(command.toString());
            while (rs.next()) {
                HashMap<String, String> temp = new HashMap<>(6);
                temp.put("ID", rs.getString("ID"));
                temp.put("NAME", rs.getString("NAME"));
                temp.put("SURNAME", rs.getString("SURNAME"));
                temp.put("DATE_START", rs.getString("DATE_START"));
                temp.put("AMOUNT", String.valueOf(rs.getDouble("AMOUNT")));
                temp.put("STATUS", rs.getString("STATUS"));
                
                Pos.add(temp);
            }
            stmt.close();

        } catch (SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return Pos;
    }

    public PaymentOrder getById(int id) {
        ResultSet rs;
        PaymentOrder out = null;
        try {
            stmt = conn.createStatement();
            String command = "SELECT * FROM PAYMENTORDER WHERE ID=" + id;
            rs = stmt.executeQuery(command);
            if (rs.next()) {
                out = new PaymentOrder();
                out.setId(rs.getInt("ID"));
                out.setId_c(rs.getInt("id_c"));
                out.setAmount(rs.getDouble("amount"));
                out.setStart_date(rs.getString("date_start"));
                out.setStatus(rs.getString("status"));
            }
            stmt.close();
        } catch (SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }

        return out;
    }

    public ArrayList<HashMap<String,String>> getAll() {
        return search("*");
    }
}
