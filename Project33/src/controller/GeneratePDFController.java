package controller;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;
import entity.Client;
import entity.PaymentOrder;
import extra.SettingReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author team33
 */

public class GeneratePDFController {

    String input;
    FileOutputStream output;
    int id;
    PaymentOrder po;
    Client cl;
    DaoPaymentOrder dpo;
    DaoClient dc;
    SettingReader sr;
    
    public GeneratePDFController() {
        sr = new SettingReader("config.ini");
        this.input = sr.getSettings().get("TEMPLATE");
        dc = new DaoClient();
        dpo = new DaoPaymentOrder();
    }

    
    public void generatePDF(int id) {
        this.id = id;
        setInfo();

        Rectangle dim;
        //Percorso del file da modificare
        try {
            //Apro il PDF con un reader
            PdfReader reader = new PdfReader(input);
            //Creo un nuovo PDF passando allo stamper un FileOutputStream. 
            //il metodo addPath lo descriverò sotto ma serve solo per cambiare nome al file originale
            PdfStamper stamper = new PdfStamper(reader, output);
            //Preparo un rettangolo che ospiterà le dimensioni della siongola pagina

            PdfContentByte over;

            dim = reader.getPageSize(reader.getPageN(1));
            //e la larghezza della pagina
            float wp = dim.getWidth();
            //Le posizioni partono dal basso
            over = stamper.getOverContent(1);
            //inizio a scrivere
            over.beginText();
            //Setto il font
            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

            over.setFontAndSize(bf, 12);

            over.setTextMatrix(110, 615);
            over.showText(cl.getName());

            over.setTextMatrix(110, 600);
            over.showText(cl.getSurname());

            over.setTextMatrix(110, 585);
            over.showText(cl.getCity_residence());

            over.setTextMatrix(110, 570);
            over.showText(cl.getStreet());

            over.setFontAndSize(bf, 16);

            over.setTextMatrix(40, 470);
            over.showText(po.getStart_date());

            over.setTextMatrix(480, 470);
            over.showText("€ " + po.getAmount());

            //Chiudo la scrittura
            over.endText();

            stamper.close();
        } catch (IOException | DocumentException e) {
            System.out.println(e.getMessage());
        }
    }


    public void setInfo(){
        po = dpo.getById(this.id);
        cl = dc.getById(po.getId_c());
        String filename = cl.getName() + cl.getSurname() + po.getStart_date() + ".pdf";
        try {
            File fp = new File(sr.getSettings().get("OUTPUT_FOLDER") + File.separator + filename);
            fp.getParentFile().mkdir();
            this.output = new FileOutputStream(fp);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}
